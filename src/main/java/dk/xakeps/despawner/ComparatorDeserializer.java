/*
    Copyright (C) 2019 Xakep_SDK

    This file is part of Despawner.

    Despawner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Despawner is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.despawner;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Comparator;

public class ComparatorDeserializer implements TypeSerializer<Comparator<ItemStack>> {
    private final ComparatorRegistry comparatorRegistry;

    public ComparatorDeserializer(ComparatorRegistry comparatorRegistry) {
        this.comparatorRegistry = comparatorRegistry;
    }

    @Nullable
    @Override
    public Comparator<ItemStack> deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        String comparatorId = value.getString();
        if (StringUtils.isBlank(comparatorId)) {
            throw new ObjectMappingException("Empty comparator id");
        }

        Comparator<ItemStack> itemStackComparator = comparatorRegistry.getById(comparatorId).orElse(null);
        if (itemStackComparator == null) {
            throw new ObjectMappingException("Unknown comparator: " + comparatorId);
        }
        return itemStackComparator;
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable Comparator<ItemStack> obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        String id = comparatorRegistry.getId(obj).orElse(null);
        if (StringUtils.isBlank(id)) {
            throw new ObjectMappingException("Unknown comparator");
        }
        value.setValue(id);
    }
}
