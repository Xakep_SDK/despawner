/*
    Copyright (C) 2019 Xakep_SDK

    This file is part of Despawner.

    Despawner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Despawner is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.despawner;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackComparators;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;

public class ComparatorRegistry {
    private final BiMap<String, Comparator<ItemStack>> comparatorMap;

    public ComparatorRegistry() {
        comparatorMap = HashBiMap.create();
        comparatorMap.put("type", ItemStackComparators.TYPE);
        comparatorMap.put("size", ItemStackComparators.SIZE);
        comparatorMap.put("type_size", ItemStackComparators.TYPE_SIZE);
        comparatorMap.put("properties", ItemStackComparators.PROPERTIES);
        comparatorMap.put("item_data", ItemStackComparators.ITEM_DATA);
        comparatorMap.put("item_data_ignore_damage", ItemStackComparators.ITEM_DATA_IGNORE_DAMAGE);
        comparatorMap.put("ignore_size", ItemStackComparators.IGNORE_SIZE);
        comparatorMap.put("all", ItemStackComparators.ALL);
    }

    public Optional<Comparator<ItemStack>> getById(String id) {
        return Optional.ofNullable(comparatorMap.get(id));
    }

    public Optional<String> getId(Comparator<ItemStack> comparator) {
        return Optional.ofNullable(comparatorMap.inverse().get(comparator));
    }

    public Map<String, Comparator<ItemStack>> getComparatorBiMap() {
        return Collections.unmodifiableMap(comparatorMap);
    }
}
