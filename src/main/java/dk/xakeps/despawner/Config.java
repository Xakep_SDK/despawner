/*
    Copyright (C) 2019 Xakep_SDK

    This file is part of Despawner.

    Despawner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Despawner is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.despawner;

import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@ConfigSerializable
public class Config {
    @Setting
    private Map<ItemType, List<ComparatorData>> items = new HashMap<>();

    public Map<ItemType, List<ComparatorData>> getItems() {
        return items;
    }

    @ConfigSerializable
    public static class ComparatorData implements Comparable<ItemStack> {
        @Setting
        private ItemStack stack;
        @Setting
        private Comparator<ItemStack> comparator;
        @Setting
        private int delay;

        public ComparatorData() {
        }

        public ComparatorData(ItemStack stack, Comparator<ItemStack> comparator, int delay) {
            this.stack = Objects.requireNonNull(stack, "stack");
            this.comparator = Objects.requireNonNull(comparator, "comparator");
            this.delay = delay;
        }

        public ItemStack getStack() {
            return stack;
        }

        public Comparator<ItemStack> getComparator() {
            return comparator;
        }

        public int getDelay() {
            return delay;
        }

        public void setDelay(int delay) {
            this.delay = delay;
        }

        @Override
        public int compareTo(ItemStack o) {
            return comparator.compare(stack, o);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ComparatorData that = (ComparatorData) o;
            return Objects.equals(stack, that.stack);
        }

        @Override
        public int hashCode() {
            return Objects.hash(stack);
        }
    }
}
