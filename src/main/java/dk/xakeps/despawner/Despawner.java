/*
    Copyright (C) 2019 Xakep_SDK

    This file is part of Despawner.

    Despawner is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Despawner is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.despawner;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializerCollection;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.type.Include;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Plugin(id = "despawner",
        name = "Despawner",
        version = "1.0",
        description = "Sets despawn delay ticks for dropped items",
        url = "https://xakeps.dk",
        authors = "Xakep_SDK")
public class Despawner {

    private final Logger logger;
    private final ConfigurationLoader<CommentedConfigurationNode> loader;
    private ComparatorRegistry comparatorRegistry;
    private CommentedConfigurationNode rootNode;
    private Config config;

    @Inject
    public Despawner(Logger logger,
                     @DefaultConfig(sharedRoot = true) ConfigurationLoader<CommentedConfigurationNode> loader) {
        this.logger = logger;
        this.loader = loader;
    }

    @Listener
    public void onGamePreInit(GameInitializationEvent event) throws IOException, ObjectMappingException {
        this.comparatorRegistry = new ComparatorRegistry();
        reloadConfig();

        CommandSpec addItemCommand = CommandSpec.builder()
                .permission("despawner.command.add.base")
                .arguments(GenericArguments.choices(Text.of("comparator"), comparatorRegistry.getComparatorBiMap()),
                        GenericArguments.integer(Text.of("ticks")),
                        GenericArguments.playerOrSource(Text.of("player")))
                .executor((src, args) -> {
                    Player player = args.<Player>getOne("player")
                            .orElseThrow(() -> new CommandException(Text.of("Player not found")));
                    Comparator<ItemStack> comparator = args.<Comparator<ItemStack>>getOne("comparator")
                            .orElseThrow(() -> new CommandException(Text.of("Comparator not found")));
                    int ticks = args.<Integer>getOne("ticks")
                            .orElseThrow(() -> new CommandException(Text.of("No ticks")));
                    ItemStack itemInHand = player.getItemInHand(HandTypes.MAIN_HAND)
                            .orElseThrow(() -> new CommandException(Text.of("No item in hand"))).copy();
                    if (itemInHand.isEmpty()) {
                        throw new CommandException(Text.of("Not item in hand"));
                    }

                    List<Config.ComparatorData> comparatorData = config.getItems().computeIfAbsent(itemInHand.getType(), itemType -> new ArrayList<>());
                    if (!comparatorData.isEmpty()) {
                        for (Config.ComparatorData comparatorDatum : comparatorData) {
                            if (comparatorDatum.getComparator().equals(comparator)
                                    && comparatorDatum.compareTo(itemInHand) == 0) {
                                comparatorDatum.setDelay(ticks);
                                src.sendMessage(Text.of("Item modified"));
                                return CommandResult.success();
                            }
                        }
                    }
                    comparatorData.add(new Config.ComparatorData(itemInHand, comparator, ticks));
                    src.sendMessage(Text.of("Item added"));
                    return CommandResult.success();
                })
                .build();

        CommandSpec removeItemCommand = CommandSpec.builder()
                .permission("despawner.command.remove.base")
                .arguments(GenericArguments.optional(GenericArguments.choices(Text.of("comparator"), comparatorRegistry.getComparatorBiMap())),
                        GenericArguments.playerOrSource(Text.of("player")))
                .executor((src, args) -> {
                    Player player = args.<Player>getOne("player")
                            .orElseThrow(() -> new CommandException(Text.of("Player not found")));
                    ItemStack itemInHand = player.getItemInHand(HandTypes.MAIN_HAND)
                            .orElseThrow(() -> new CommandException(Text.of("No item in hand")));
                    Optional<Comparator<ItemStack>> comparator = args.<Comparator<ItemStack>>getOne("comparator");
                    Collection<Config.ComparatorData> comparatorData = config.getItems().get(itemInHand.getType());
                    if (comparatorData == null || comparatorData.isEmpty()) {
                        src.sendMessage(Text.of("No items found"));
                        return CommandResult.empty();
                    }
                    Iterator<Config.ComparatorData> iterator = comparatorData.iterator();
                    int i = 0;
                    while (iterator.hasNext()) {
                        Config.ComparatorData comparatorDatum = iterator.next();
                        if (comparator.isPresent()) {
                            if (comparator.get().compare(comparatorDatum.getStack(), itemInHand) == 0) {
                                i++;
                                iterator.remove();
                            }
                            continue;
                        }
                        if (comparatorDatum.compareTo(itemInHand) == 0) {
                            i++;
                            iterator.remove();
                        }
                    }
                    if (comparatorData.isEmpty()) {
                        config.getItems().remove(itemInHand.getType());
                    }
                    src.sendMessage(Text.of(i + " items removed"));
                    return CommandResult.successCount(i);
                })
                .build();

        CommandSpec saveCommand = CommandSpec.builder()
                .permission("despawner.command.save.base")
                .executor((src, args) -> {
                    try {
                        rootNode.setValue(TypeToken.of(Config.class), config);
                        loader.save(rootNode);
                        src.sendMessage(Text.of("Config saved"));
                        return CommandResult.success();
                    } catch (ObjectMappingException | IOException e) {
                        logger.error("Error while saving config", e);
                        src.sendMessage(Text.of("Error while saving config"));
                        return CommandResult.empty();
                    }
                })
                .build();

        CommandSpec rootCommand = CommandSpec.builder()
                .permission("despawner.command.root.base")
                .child(addItemCommand, "add")
                .child(removeItemCommand, "remove")
                .child(saveCommand, "save")
                .build();

        Sponge.getCommandManager().register(this, rootCommand, "despawner");
    }


    @Include({DropItemEvent.Dispense.class, DropItemEvent.Close.class, DropItemEvent.Destruct.class, DropItemEvent.Custom.class})
    @Listener
    public <E extends DropItemEvent & SpawnEntityEvent> void onEntityDrop(E event) {
        for (Entity entity : event.getEntities()) {
            if (!(entity instanceof Item)) {
                continue;
            }
            Optional<ItemStackSnapshot> item = entity.get(Keys.REPRESENTED_ITEM);
            if (!item.isPresent()) {
                return;
            }

            Collection<Config.ComparatorData> comparatorData = config.getItems().get(item.get().getType());
            if (comparatorData == null || comparatorData.isEmpty()) {
                continue;
            }
            ItemStack stack = item.get().createStack();
            for (Config.ComparatorData configItem : comparatorData) {
                if (configItem.compareTo(stack) == 0) {
                    if (configItem.getDelay() <= -1) {
                        entity.offer(Keys.INFINITE_DESPAWN_DELAY, true);
                    } else {
                        entity.offer(Keys.DESPAWN_DELAY, configItem.getDelay());
                    }
                    break;
                }
            }

        }
    }

    @Listener
    public void onReload(GameReloadEvent event) throws IOException, ObjectMappingException {
        reloadConfig();
    }

    private void reloadConfig() throws IOException, ObjectMappingException {
        ConfigurationOptions opts = ConfigurationOptions.defaults();
        TypeSerializerCollection collection = opts.getSerializers().newChild()
                .registerType(new TypeToken<Comparator<ItemStack>>() {}, new ComparatorDeserializer(comparatorRegistry));
        opts = opts.setSerializers(collection);
        this.rootNode = loader.load(opts);
        this.config = rootNode.getValue(TypeToken.of(Config.class), new Config());
    }
}
